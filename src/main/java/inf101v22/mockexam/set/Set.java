package inf101v22.mockexam.set;

import java.util.ArrayList;
import java.util.Iterator;

public class Set<T> implements ISet<T> {

    private ArrayList<T> set;

    public Set() {
        this.set = new ArrayList<>();
    }

    @Override
    public Iterator<T> iterator() {
        ArrayList<T> iterator = new ArrayList<>();
        
        for (int i = 0; i< set.size(); i++) {
            iterator.add(set.get(i));
        }
        return iterator.iterator();
    }

    @Override
    public int size() {
        return set.size();
    }

    @Override
    public void add(T element) {
        set.add(element);
    }

    @Override
    public void addAll(Iterable<T> other) {
        for ( T object : other) {
            if (set.contains(object) != true) {
                set.add(object);
            }
        }
    }

    @Override
    public void remove(T element) {
        set.remove(element);
    }

    @Override
    public boolean contains(T element) {
        if (set.contains(element)) {
            return true;
        }
        return false;
    }

    @Override
    public ISet<T> union(ISet<T> other) {
        Set<T> union = new Set<>();

        for (T element : this.set) {
            union.add(element);
        }
        for (T element : other) {
            if (union.contains(element) != true) {
                union.add(element);
            }
        }
        return union;
    }

    @Override
    public ISet<T> intersection(ISet<T> other) {
        Set<T> intersection = new Set<>();

        for (T element : this.set) {
            if (set.contains(element) && other.contains(element)) {
                intersection.add(element);
            }
        }
        for (T element : other) {
            if (other.contains(element) && this.set.contains(element)) {
                intersection.add(element);
            }
        }
        return intersection;
    }

    @Override
    public ISet<T> complement(ISet<T> other) {
        Set<T> complement = new Set<>();
        complement.addAll(other);
        complement.addAll(set);

        for ( T element : complement) {
            if (set.contains(element)) {
                set.remove(element);
            }
        }
        return complement;
        
    }

    @Override
    public ISet<T> copy() {
        ISet<T> copySet = new Set<>();
        for (T element : this.set) {
            copySet.add(element);
        }
        return copySet;
    }
    
}
