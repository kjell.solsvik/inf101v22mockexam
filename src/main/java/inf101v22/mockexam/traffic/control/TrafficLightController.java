package inf101v22.mockexam.traffic.control;

import java.awt.event.ActionEvent;

import javax.swing.Timer;

import inf101v22.mockexam.observable.ControlledObservable;
import inf101v22.mockexam.observable.Observable;
import inf101v22.mockexam.traffic.model.TrafficLightControllable;

public class TrafficLightController implements ITrafficGuiController {
    
    private TrafficLightControllable model;
    private ControlledObservable<Boolean> isPaused =  new ControlledObservable<Boolean>(true);
    private Timer timer;

    public TrafficLightController(TrafficLightControllable model) {
        this.model = model;
        this.timer = new Timer(model.minMillisInCurrentState(), this::timerFired);
        timer.start();
    }

    private void timerFired(ActionEvent e) {
        model.goToNextState();
        timer.setInitialDelay(model.minMillisInCurrentState());
        timer.restart();
    }

    @Override
    public Observable<Boolean> isPaused() {
        return isPaused;
    }

    @Override
    public void pausePressed(ActionEvent e) {
        this.isPaused.setValue(!(this.isPaused.getValue()));
        
        timer.stop();
        
    }

    @Override
    public void startPressed(ActionEvent e) {
        this.isPaused.setValue(!(this.isPaused.getValue()));
        timer.setInitialDelay(model.minMillisInCurrentState());
        timer.start();
    }
}
