package inf101v22.mockexam.traffic.view;

import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JComponent;

import inf101v22.mockexam.traffic.model.TrafficLightViewable;

public class TrafficLightView extends JComponent {
    

    public TrafficLightView (TrafficLightViewable model) {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        LampView redLight = new LampView(model.redIsOn(), Color.RED);
        LampView greenLight = new LampView(model.greenIsOn(), Color.GREEN);
        LampView yellowLight = new LampView(model.yellowIsOn(), Color.YELLOW);
        this.add(redLight);
        this.add(yellowLight);
        this.add(greenLight);

    }
}
