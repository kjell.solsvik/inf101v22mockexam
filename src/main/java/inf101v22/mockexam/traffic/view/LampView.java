package inf101v22.mockexam.traffic.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;
import inf101v22.mockexam.observable.Observable;

public class LampView extends JComponent {
    
    private Observable<Boolean> isOn;
    private Color color;

    public LampView(Observable<Boolean> isOn, Color color) {
        this.isOn = isOn;
        this.color = color;
        isOn.addObserver(this::repaint);
    }

    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        drawLight(g2);
    }

    private void drawLight(Graphics2D g2) {
        if (isOn.getValue() == true) {
            g2.setColor(color);
        } 
        else {
            g2.setColor(color.darker().darker().darker().darker());
        }
        g2.fillOval(0, 0, getWidth(), getHeight());
    }

    @Override
    public Dimension preferredSize() {
        return new Dimension(50, 50);
    }
}
