package inf101v22.mockexam.traffic.model;

import inf101v22.mockexam.observable.ControlledObservable;
import inf101v22.mockexam.observable.Observable;

public class TrafficLightModel implements TrafficLightControllable {

    private int minMillisInCurrentState = 2000;
    private ControlledObservable<Boolean> redIsOn = new ControlledObservable<>(true);
    private ControlledObservable<Boolean> yellowIsOn = new ControlledObservable<>(false);
    private ControlledObservable<Boolean> greenIsOn = new ControlledObservable<>(false);

    public TrafficLightModel() {
    }
    
    

    @Override
    public Observable<Boolean> redIsOn() {
        return this.redIsOn;
    }

    @Override
    public Observable<Boolean> yellowIsOn() {
        return this.yellowIsOn;
    }

    @Override
    public Observable<Boolean> greenIsOn() {
        return this.greenIsOn;
    }

    //Rødt, rødt og gult, grønnt, gult, rødt igjen.
     // Red state:    2000 ms
        // R+Y state:     500 ms
        // Green state:  2000 ms
        // Yellow state: 1000 ms

    @Override
    public void goToNextState() {
        if (redIsOn.getValue() == true && yellowIsOn.getValue() == true) {
            redIsOn.setValue(false);
            yellowIsOn.setValue(false);
            greenIsOn.setValue(true);
            this.minMillisInCurrentState = 2000;
        }
        else if (redIsOn.getValue() == true) {
            yellowIsOn.setValue(true);
            this.minMillisInCurrentState = 500;
        }
        else if ( yellowIsOn.getValue() == true) {
            yellowIsOn.setValue(false);
            redIsOn.setValue(true);
            this.minMillisInCurrentState = 2000;
        }
        else if (greenIsOn.getValue() == true) {
            greenIsOn.setValue(false);
            yellowIsOn.setValue(true);
            this.minMillisInCurrentState = 1000;
        }
    }


    
    @Override
    public int minMillisInCurrentState() {
        return this.minMillisInCurrentState;
    }
    
}
