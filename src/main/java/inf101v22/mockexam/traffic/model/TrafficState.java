package inf101v22.mockexam.traffic.model;

public enum TrafficState {
    RED, GREEN, YELLOW, GREENANDYELLOW
}
